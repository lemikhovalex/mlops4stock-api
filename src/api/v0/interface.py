import datetime

from fastapi import APIRouter, Depends
from pydantic import BaseModel, Field

from api.service_getters import (get_imoex_http_data_connector,
                                 get_pipeline_gmv, get_pipeline_gmv_l2)
from core.config import ModelSetting
from services.optimization_pipeline import IPortOptimizer
from services.stock_data import BaseStockDataProvider

router = APIRouter()
model_settings = ModelSetting()


def get_today() -> datetime.date:
    return datetime.datetime.now().date()


class OptInterfaceInp(BaseModel):
    dt_to: datetime.date = Field(default_factory=get_today)
    money: float = 1_000


class TickRes(BaseModel):
    name: str
    val: float


class OptInterfaceOut(BaseModel):
    vals: list[TickRes]
    free_money: float


@router.post("/optimize_gmv")
async def optimize(
    data: OptInterfaceInp,
    stock_data_conn: BaseStockDataProvider = Depends(get_imoex_http_data_connector),
    pipeline: IPortOptimizer = Depends(get_pipeline_gmv),
) -> OptInterfaceOut:
    tickers = model_settings.tickers
    df = await stock_data_conn.get_tickers_data(
        tickers=tickers,
        dt_from=data.dt_to - datetime.timedelta(days=model_settings.days_for_inference),
        dt_to=data.dt_to,
    )
    res, free = pipeline.optimize(data=df, money=data.money)
    return OptInterfaceOut(
        vals=[TickRes(name=k, val=v) for k, v in res.items()],
        free_money=free,
    )


@router.post("/optimize_gmv_l2")
async def optimize_custom(
    data: OptInterfaceInp,
    stock_data_conn: BaseStockDataProvider = Depends(get_imoex_http_data_connector),
    pipeline: IPortOptimizer = Depends(get_pipeline_gmv_l2),
) -> OptInterfaceOut:
    tickers = model_settings.tickers
    df = await stock_data_conn.get_tickers_data(
        tickers=tickers,
        dt_from=data.dt_to - datetime.timedelta(days=model_settings.days_for_inference),
        dt_to=data.dt_to,
    )
    res, free = pipeline.optimize(data=df, money=data.money)
    return OptInterfaceOut(
        vals=[TickRes(name=k, val=v) for k, v in res.items()],
        free_money=free,
    )
