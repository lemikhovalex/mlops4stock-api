import typing as tp

from fastapi import Depends

from core.config import ModelSetting
from services.model import IModel, MLFlowModel
from services.optimization_pipeline import (ClassificationBasedOptimizerGMV,
                                            ClassificationBasedOptimizerGMVL2)
from services.stock_data import ImoexHttpDataConnector

model_setting = ModelSetting()

model = MLFlowModel(
    model_name=model_setting.name,
    model_stage=model_setting.stage,
)


def get_imoex_http_data_connector() -> tp.Generator[ImoexHttpDataConnector, None, None]:
    yield ImoexHttpDataConnector()


def get_model() -> tp.Generator[IModel, None, None]:
    yield model


def get_pipeline_gmv(
    model: IModel = Depends(get_model),
) -> tp.Generator[ClassificationBasedOptimizerGMV, None, None]:
    yield ClassificationBasedOptimizerGMV(model=model)


def get_pipeline_gmv_l2(
    model: IModel = Depends(get_model),
) -> tp.Generator[ClassificationBasedOptimizerGMVL2, None, None]:
    yield ClassificationBasedOptimizerGMVL2(model=model)
