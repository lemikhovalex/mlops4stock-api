import os

from pydantic import BaseSettings, Field


class ApiSetting(BaseSettings):
    host: str = Field("127.0.0.1", env="API_HOST")
    port: int = Field(8000, env="API_PORT")
    debug: bool = False


class ModelSetting(BaseSettings):
    name: str = Field(..., env="MODEL_NAME")
    stage: str = Field("Production", env="MODEL_STAGE")
    days_for_inference: int = Field(..., env="DAYS_FOR_INFERENCE")
    tickers: list[str] = [str(x) for x in os.getenv("ALLOWED_TICKERS", "").split(",")]
