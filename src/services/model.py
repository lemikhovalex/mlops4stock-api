import logging
import typing as tp
from abc import ABC, abstractmethod

import mlflow
import pandas as pd


class IModel(ABC):
    @abstractmethod
    def predict(self, data: tp.Any) -> dict[tp.Any, tp.Any]:
        ...

    @abstractmethod
    def transform(self, data: tp.Any) -> tp.Any:
        ...


class MLFlowModel(IModel):
    def __init__(
        self,
        model_name: str,
        model_stage: str,
    ):
        model = mlflow.pyfunc.load_model(
            "models:/{name}/{stage}".format(name=model_name, stage=model_stage)
        )
        self.model = model.unwrap_python_model()

    def predict(self, data: pd.DataFrame) -> dict[tp.Any, tp.Any]:
        out = self.model.predict(data)
        return out

    def transform(self, data: pd.DataFrame) -> tp.Any:
        logging.debug(data)
        return self.model.transform(data)
