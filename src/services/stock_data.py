import asyncio
import datetime
import logging
import typing as tp
from abc import ABC, abstractmethod

import aiohttp
import aiomoex
import pandas as pd


class BaseStockDataProvider(ABC):
    @abstractmethod
    async def get_tickers_data(
        self,
        tickers: list[str],
        dt_from: datetime.date,
        dt_to: datetime.date,
    ) -> tp.Any:
        ...


class ImoexHttpDataConnector(BaseStockDataProvider):
    def __init__(
        self,
        n_sessions: int = 2,
        format: str = "close",
        cutoff: float = 0.01,
    ):
        self.n_sessions = n_sessions
        self.format = format
        self.cutoff = cutoff

    async def _get_single_ticker(
        self,
        ticker: str,
        start: datetime.date,
        end: datetime.date,
        session: aiohttp.ClientSession,
        sem: asyncio.Semaphore,
        delay: float,
        interval: int = 24,
    ) -> list[dict[tp.Any, tp.Any]]:
        """get OHLCV data form MOEX API"""

        async with sem:
            await asyncio.sleep(delay)
            data = await aiomoex.get_market_candles(
                session,
                security=ticker,
                interval=interval,
                start=start.strftime("%Y-%m-%d"),
                end=end.strftime("%Y-%m-%d"),
            )
        return data

    def _get_df_for_ticker(
        self, ticker_name: str, data: list[dict[tp.Any, tp.Any]]
    ) -> pd.DataFrame:
        df = pd.DataFrame(
            (
                {
                    "dttm": datetime.datetime.strptime(d["begin"], "%Y-%m-%d %H:%M:%S"),
                    ticker_name: d[self.format],
                }
            )
            for d in data
        )
        logging.info(f"we fetched df with shape {df.shape}")
        df["Date"] = pd.to_datetime(df["dttm"]).dt.date
        df.index = df["Date"]  # type:ignore
        df.drop(columns=["dttm", "Date"], inplace=True)
        return df

    async def get_tickers_data(
        self,
        tickers: list[str],
        dt_from: datetime.date,
        dt_to: datetime.date,
    ) -> pd.DataFrame:
        ...
        sem = asyncio.Semaphore(self.n_sessions)

        async with aiohttp.ClientSession() as session:
            raw_list = await asyncio.gather(
                *[
                    self._get_single_ticker(
                        ticker, dt_from, dt_to, session, sem, delay=1
                    )
                    for ticker in tickers
                ]
            )
        dfs = (
            self._get_df_for_ticker(ticker_name=t, data=raw)
            for (t, raw) in zip(tickers, raw_list)
        )
        out = pd.concat(dfs, axis=1)
        out = self._clean_data(data=out)
        return out

    def _clean_data(self, data: pd.DataFrame) -> pd.DataFrame:
        nan_col = data.loc[:, data.isna().mean() > self.cutoff].columns.tolist()

        if nan_col:
            data.drop(nan_col, axis=1, inplace=True)

        # drop rows
        nan_row = data.isna().any(axis=1).sum()

        if nan_row:
            data.dropna(axis=0, inplace=True)
        return data
