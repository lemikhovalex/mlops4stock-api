PROJECT_NAME = mlops4stocks
PYTHON_INTERPRETER = python

# Environment variables for project
ENV	:= .env
include $(ENV)


## Install Python Dependencies
install:
	$(PYTHON_INTERPRETER) -m pip install -U pip setuptools wheel
	$(PYTHON_INTERPRETER) -m pip install -r requirements.txt


## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete


## Lint using flake8
lint:
	python -m black src
	python -m isort src
	flake8 src
	mypy src

## to restart containers as clean slate
restart-containers:
	docker-compose down && \
	docker-compose up -d --build --force-recreate